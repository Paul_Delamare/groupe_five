-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : ven. 18 nov. 2022 à 09:31
-- Version du serveur : 10.4.25-MariaDB
-- Version de PHP : 8.1.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `vaccination`
--

-- --------------------------------------------------------

--
-- Structure de la table `avis`
--

CREATE TABLE `avis` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `content` text NOT NULL,
  `status_avis` varchar(30) NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `avis`
--

INSERT INTO `avis` (`id`, `user_id`, `content`, `status_avis`, `created_at`) VALUES
(3, 2, 'Je poste un avis constructif', 'publish', '2022-11-15 15:55:44');

-- --------------------------------------------------------

--
-- Structure de la table `contact`
--

CREATE TABLE `contact` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `created_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `contact`
--

INSERT INTO `contact` (`id`, `user_id`, `email`, `content`, `created_at`) VALUES
(6, NULL, 'melvin.delorme12@gmail.com', 'Je teste des choses', '2022-11-15 15:55:13');

-- --------------------------------------------------------

--
-- Structure de la table `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(150) NOT NULL,
  `prenom` varchar(255) NOT NULL,
  `email` varchar(150) NOT NULL,
  `age` int(3) NOT NULL,
  `genre` varchar(150) NOT NULL,
  `password` varchar(255) NOT NULL,
  `status` varchar(30) NOT NULL,
  `token` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `user`
--

INSERT INTO `user` (`id`, `name`, `prenom`, `email`, `age`, `genre`, `password`, `status`, `token`, `created_at`, `modified_at`) VALUES
(2, 'Delorme', 'Melvin', 'melvin.delorme12@gmail.com', 19, 'homme', '$2y$10$N9T4VUvE2c0oxXDBKuWBweLZaT7lbkVYehOg7rFKFh3yK1l/TAVCm', 'admin', 'sqePmhFfBAyNw6r9llfKzSoFLRjNOMAR71EAcfvsdtW01O4vCljXFJKTcYjrStn3DnDprUhY4zgda2U0', '2022-11-09 12:07:11', '2022-11-10 09:29:41');

-- --------------------------------------------------------

--
-- Structure de la table `vaccin`
--

CREATE TABLE `vaccin` (
  `id` int(11) NOT NULL,
  `title` varchar(150) NOT NULL,
  `content` text NOT NULL,
  `img` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vaccin`
--

INSERT INTO `vaccin` (`id`, `title`, `content`, `img`, `created_at`, `modified_at`) VALUES
(14, 'Coqueluche', 'La coqueluche est une maladie provoquée par la bactérie Bordetella pertussis. Elle se manifeste par des accès de toux, des difficultés à respirer (surtout à l’inspiration), et des vomissements provoqués…', '', '2022-11-15 15:32:31', '0000-00-00 00:00:00'),
(15, 'Coronavirus (COVID-19)', 'Le COVID-19 est une maladie provoquée par le coronavirus SARS-CoV-2. Il peut se manifester de différentes manières, mais les symptômes les plus courants sont : Symptômes d’affection aiguë des voies respiratoires…', '', '2022-11-15 15:32:49', '0000-00-00 00:00:00'),
(16, 'Diphtérie', 'La diphtérie est une maladie provoquée par la bactérie Corynebacterium diphtheriae. Elle n’existe que chez l’Homme et est transmise par des gouttelettes de sécrétions lors de toux ou d’éternuement, plus rarement par le contact…', '', '2022-11-15 15:33:06', '0000-00-00 00:00:00'),
(17, 'Fièvre jaune', 'La fièvre jaune est une maladie virale, parfois mortelle, transmise par les moustiques. Dans les 3 à 6 jours suivant l\'infection survient une fièvre importante, des frissons, des maux de tête…', '', '2022-11-15 15:33:27', '0000-00-00 00:00:00'),
(18, 'Fièvre typhoïde', 'La fièvre typhoïde est une maladie grave provoquée par la bactérie Salmonella typhi. L\'infection se transmet par l\'eau potable ou des aliments (crustacés, crèmes glacées, pâtisseries, sauces, etc.) contaminés par…', '', '2022-11-15 15:33:42', '0000-00-00 00:00:00'),
(19, 'Grippe (influenza)', 'La grippe est une maladie provoquée par les virus Influenza A et Influenza B. Les premiers symptômes de la grippe incluent notamment une sensation de malaise général, un abattement, une brusque poussée…', '', '2022-11-15 15:34:06', '0000-00-00 00:00:00'),
(20, 'Haemophilus influenzae b', 'Haemophilus influenzae type b (Hib) est le nom de la bactérie qui provoque chez les nourrissons et les petits enfants une méningite purulente ou une inflammation de l’épiglotte pouvant conduire à…', '', '2022-11-15 15:34:29', '0000-00-00 00:00:00'),
(21, 'Hépatite A', 'Le virus de l’hépatite A est transmis par de l’eau, des jus ou des aliments insuffisamment cuits (salades, fruits non pelés, fruits de mer, glaçons), essentiellement dans des pays où les…', '', '2022-11-15 15:34:46', '0000-00-00 00:00:00'),
(22, 'Hépatite B', 'Le virus de l’hépatite B s’attrape par contact avec le sang, ou lors de rapports sexuels non protégés avec une personne infectée. La phase aiguë se manifeste par une jaunisse (coloration…', '', '2022-11-15 15:35:01', '0000-00-00 00:00:00'),
(23, 'HPV – virus du papillome humain', 'Il existe plus d’une centaine de virus du papillome humain (HPV) qui infectent la peau ou les muqueuses génitales. Ces virus se transmettent très facilement au cours des relations sexuelles,…', '', '2022-11-15 15:35:13', '0000-00-00 00:00:00'),
(24, 'Méningo-encéphalite à tiques', 'Les tiques peuvent être infectées par plusieurs microbes et donc transmettre diverses maladies. Les deux plus importantes sont la borréliose (maladie de Lyme, provoquée par la bactérie Borrelia burgdorferi) et…', '', '2022-11-15 15:35:25', '0000-00-00 00:00:00'),
(25, 'Méningocoques', 'Dans la population, environ 15% des personnes portent la bactérie Neisseria meningitidis (méningocoque) dans le nez ou la gorge, sans être malades. Il y a cinq principaux types de méningocoques…', '', '2022-11-15 15:35:38', '0000-00-00 00:00:00'),
(26, 'Oreillons', 'Les oreillons sont provoqués par un virus (de la famille des Paramyxovirus) qui fait gonfler les glandes salivaires, donnant l’apparence d’avoir les joues d’un hamster. La maladie est souvent bénigne…', '', '2022-11-15 15:35:51', '0000-00-00 00:00:00'),
(27, 'Pneumocoques', 'Les pneumocoques sont des bactéries qui peuvent engendrer de nombreuses maladies plus ou moins graves : des otites moyennes, désagréables mais ne menaçant pas la vie, des pneumonies pouvant provoquer des…', '', '2022-11-15 15:36:06', '0000-00-00 00:00:00'),
(28, 'Poliomyélite', 'La poliomyélite ou paralysie infantile est due au poliovirus qui est transmis par le contact avec des excréments (mains souillées) ou de l’eau contaminée. Beaucoup de personnes attrapent cette infection…', '', '2022-11-15 15:36:52', '0000-00-00 00:00:00'),
(29, 'Rage', 'La rage est une maladie provoquée par un virus (famille des Rhabdoviridae, genre Lyssavirus) et presque toujours mortelle. Elle s\'attrape par morsure/griffure ou contact avec la salive d\'animaux infectés. Il n’existe aucun…', '', '2022-11-15 15:37:05', '0000-00-00 00:00:00'),
(30, 'Rotavirus', 'À l’échelon mondial, les rotavirus sont la cause la plus fréquente de gastroentérites sévères, déshydratantes chez les enfants de moins de 5 ans. Ils provoquent des vomissements, des diarrhées et un…', '', '2022-11-15 15:37:23', '0000-00-00 00:00:00'),
(31, 'Rougeole', 'La rougeole est une maladie provoquée par un virus. Elle commence généralement par un simple rhume, suivi de toux et d’une irritation des yeux. Après quelques jours, la fièvre monte…', '', '2022-11-15 15:37:35', '0000-00-00 00:00:00'),
(32, 'Rubéole', 'La rubéole est due au Rubivirus, qui provoque de petites taches roses sur la peau, des ganglions dans le cou, et parfois une conjonctivite. Chez les adultes, elle peut…', '', '2022-11-15 15:37:45', '0000-00-00 00:00:00'),
(33, 'Tétanos', 'La bactérie responsable du tétanos (Clostridium tetani) se trouve partout, et en particulier dans la terre et la poussière ramenée de l’extérieur. La bactérie sécrète une toxine qui provoque la…', '', '2022-11-15 15:38:00', '0000-00-00 00:00:00'),
(34, 'Tuberculose', 'La tuberculose est provoquée par des bactéries (Mycobacterium tuberculosum) transmises par les gouttelettes produites lorsqu’une personne atteinte de tuberculose pulmonaire tousse. Cette contamination nécessite généralement un contact de plusieurs heures…', '', '2022-11-15 15:38:13', '0000-00-00 00:00:00'),
(35, 'Varicelle', 'La varicelle est provoquée par le virus de la varicelle-zona. Elle se transmet de personne à personne, et est très contagieuse. La varicelle s’attrape le plus souvent durant l’enfance. Elle…', '', '2022-11-15 15:38:28', '0000-00-00 00:00:00'),
(36, 'Variole du singe', 'La variole du singe est provoquée par un virus très proche du virus de la variole \"ordinaire\" – maladie éradiquée au niveau mondial depuis 1980. Ces deux virus font partie du…', '', '2022-11-15 15:38:47', '0000-00-00 00:00:00'),
(37, 'Zona (herpès zoster)', 'Le zona est la conséquence de la réactivation du virus varicelle-zona (VVZ). Toute personne qui a déjà eu la varicelle peut développer un zona. On estime qu’une personne sur quatre…', '', '2022-11-15 15:38:58', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Structure de la table `vaccin_status`
--

CREATE TABLE `vaccin_status` (
  `id` int(11) NOT NULL,
  `vaccin_id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `date_done` datetime DEFAULT NULL,
  `date_todo` datetime DEFAULT NULL,
  `status` varchar(30) NOT NULL,
  `doses` int(11) NOT NULL,
  `created_at` datetime NOT NULL,
  `modified_at` datetime NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `vaccin_status`
--

INSERT INTO `vaccin_status` (`id`, `vaccin_id`, `user_id`, `date_done`, `date_todo`, `status`, `doses`, `created_at`, `modified_at`) VALUES
(29, 14, 2, NULL, '2022-11-23 00:00:00', 'en attente', 2, '2022-11-16 17:23:17', '0000-00-00 00:00:00'),
(30, 19, 2, NULL, '2022-11-25 00:00:00', 'en attente', 2, '2022-11-16 18:11:01', '0000-00-00 00:00:00'),
(31, 27, 2, NULL, '2022-11-25 00:00:00', 'en attente', 2, '2022-11-16 18:11:35', '0000-00-00 00:00:00'),
(32, 26, 2, NULL, '2022-11-25 00:00:00', 'en attente', 2, '2022-11-16 18:14:10', '0000-00-00 00:00:00'),
(33, 35, 2, '2022-11-16 00:00:00', NULL, 'effectué', 1, '2022-11-17 12:55:39', '0000-00-00 00:00:00'),
(34, 14, 2, NULL, '2022-11-23 00:00:00', 'en attente', 0, '2022-11-17 13:10:32', '0000-00-00 00:00:00'),
(35, 29, 2, '2022-11-16 00:00:00', '2022-11-19 00:00:00', 'en attente', 6, '2022-11-17 13:21:59', '0000-00-00 00:00:00');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `avis`
--
ALTER TABLE `avis`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contact`
--
ALTER TABLE `contact`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vaccin`
--
ALTER TABLE `vaccin`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `vaccin_status`
--
ALTER TABLE `vaccin_status`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `avis`
--
ALTER TABLE `avis`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `contact`
--
ALTER TABLE `contact`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT pour la table `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT pour la table `vaccin`
--
ALTER TABLE `vaccin`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=38;

--
-- AUTO_INCREMENT pour la table `vaccin_status`
--
ALTER TABLE `vaccin_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=36;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
