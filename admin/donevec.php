<?php
session_start();

require '../inc/pdo.php';
require '../inc/function.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['status'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}



if (!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $post = getVecById($id);
    if (empty($post)) {
        die('404');
    } else {
        // UPDATE
        $sql = "UPDATE vaccin_status SET status = 'effectué' WHERE id = :id";
        $query = $pdo->prepare($sql);
        $query->bindValue('id', $id, PDO::PARAM_STR);
        $query->execute();
        header('Location: gestion_vaccinencours.php');
    }
} else {
    die('404');
}
