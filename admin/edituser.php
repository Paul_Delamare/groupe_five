<?php
session_start();

require '../inc/pdo.php';
require '../inc/function.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['status'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}


if(!empty($_GET['id']) && is_numeric($_GET['id'])) {
    $id = $_GET['id'];
    $user = getUserById($id);
    if(empty($user)) {
        header('location:index.php');
    }
} else {
    header('location:index.php');
}

$errors = array();
$success = false;
$genres=array(
    'femme'=>'Femme',
    'homme'=>'Homme',
    'autre'=>'Autre'
);

if (!empty($_POST['submitted'])) {
    // Faille XSS
    $name = cleanXss('name');
    $prenom= cleanXss('prenom');
    $email = cleanXss('email');
    $genre= cleanXss('genre');
    $age= cleanXss('age');

    $errors = validationText($errors, $name, 'name', 3, 150);
    $errors= validationText($errors, $prenom, 'prenom', 2, 150);
    $errors = validationEmail($errors, $email);

    if (count($errors) == 0){
        updateUser($id, $name, $prenom, $email, $genre, $age);
        $success = true;
        header('Location: gestion_users.php');
    }
}
?>
<?php include ('inc/sidebar.php'); ?>
<?php include ('inc/header.php'); ?>

            <!-- Begin Page Content -->
            <div class="container-fluid">

                <!-- Page Heading -->
                <h1 class="h3 mb-2 text-gray-800">Modifier un utilisateur</h1>

                <!-- DataTales Example -->
                <div class="card shadow mb-4">
                    <form style="margin: 1rem" action="" method="post" class="wrapform" novalidate>

                        <div class="form_nomPrenom">
                            <div style="display: flex; flex-direction: column; padding-bottom: 3rem" class="form_nom">
                                <input name="name" id="name" placeholder="Nom" type="text" value="<?php getPostValue('name', $user['name']); ?>">
                                <span class="errors"><?php viewError($errors, 'name'); ?></span>
                            </div>
                            <div style="display: flex; flex-direction: column; padding-bottom: 3rem" class="form_prenom">
                                <input type="text" id="prenom" name="prenom" placeholder="Prénom" value="<?php getPostValue('prenom', $user['prenom']); ?>">
                                <span class="errors"><?php viewError($errors, 'prenom');  ?></span>
                            </div>
                        </div>

                        <div style="display: flex; flex-direction: column; padding-bottom: 3rem" class="form_age">
                            <input name="age" id="age" placeholder="Age" type="number" min="0" max="123" value="<?php getPostValue('age', $user['age']); ?>">
                            <span class="errors"><?php viewError($errors, 'age'); ?></span>
                        </div>

                        <div style="display: flex; flex-direction: column; padding-bottom: 3rem" class="form_genre">
                            <select name="genre" id="genre">
                                <option value="">__ selectionnez un genre __</option>
                                <?php foreach ($genres as $key => $value) { ?>
                                    <option value="<?php echo $key; ?>"<?php
                                    if(!empty($_POST['genre']) && $_POST['genre'] === $key) {
                                        echo ' selected';
                                    }
                                    ?>><?php echo $value; ?></option>
                                <?php } ?>
                            </select>
                            <span class="error"><?php if(!empty($errors['genre'])) { echo $errors['genre']; } ?></span>
                        </div>

                        <div style="display: flex; flex-direction: column; padding-bottom: 3rem" class="form_email">
                            <input name="email" id="email" placeholder="Email" type="email" value="<?php getPostValue('email', $user['email']); ?>">
                            <span class="errors"><?php viewError($errors, 'email'); ?></span>
                        </div>

                        <div style="display: flex; justify-content: center" class="form_input">
                            <input name="submitted" type="submit" value="Modifier">
                        </div>
                    </form>
                </div>

            </div>
            <!-- /.container-fluid -->

        </div>
        <!-- End of Main Content -->

<?php include ('inc/footer.php'); ?>