<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">

    <title>SB Admin 2 - Dashboard</title>

    <!-- Custom fonts for this template-->
    <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
    <link
        href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
        rel="stylesheet">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.0/css/all.min.css" integrity="sha512-xh6O/CkQoPOWDdYTDqeRdPCVd1SpvCA9XXcUnZS2FmJNp1coAFzvtCN9BmamE+4aHK8yyUHUSCcJHgXloTyT2A==" crossorigin="anonymous" referrerpolicy="no-referrer" />

    <!-- Custom styles for this template-->
    <link href="css/sb-admin-2.min.css" rel="stylesheet">

    <!--  JS  -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>

</head>

<body id="page-top">

<!-- Page Wrapper -->
<div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

        <!-- Sidebar - Brand -->
        <a class="sidebar-brand d-flex align-items-center justify-content-center" href="index.php">
            <img style="width: 70%;" src="../asset/image/Logo.png" alt="logo">
        </a>

        <!-- Divider -->
        <hr class="sidebar-divider my-0">

        <!-- Nav Item - Dashboard -->
        <li class="nav-item active">
            <a class="nav-link" href="index.php">
                <i class="fas fa-fw fa-tachometer-alt"></i>
                <span>Dashboard</span></a>
        </li>
        <li class="nav-item active">
            <a class="nav-link" href="../index.php">
                <i class="fa-solid fa-house"></i>
                <span>Accueil Front</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider">

        <!-- Heading -->
        <div class="sidebar-heading">
            Gestion Administration
        </div>

        <!-- Nav Item - Gestion Users -->
        <li class="nav-item">
            <a class="nav-link" href="gestion_users.php">
                <i class="fa-solid fa-user"></i>
                <span>Gestion des utilisateurs</span></a>
        </li>

        <!-- Nav Item - Gestion Vaccin -->
        <li class="nav-item">
            <a class="nav-link" href="gestion_vaccins.php">
                <i class="fa-solid fa-syringe"></i>
                <span>Gestion des Vaccins</span></a>
        </li>

        <!-- Nav Item - Gestion Vaccin en cours -->
        <li class="nav-item">
            <a class="nav-link" href="gestion_vaccinencours.php">
                <i class="fa-regular fa-star"></i>
                <span>Gestion Vaccin en cours</span></a>
        </li>

        <!-- Nav Item - Gestion Contact -->
        <li class="nav-item">
            <a class="nav-link" href="gestion_contacts.php">
                <i class="fa-solid fa-phone"></i>
                <span>Gestion Contact</span></a>
        </li>

        <!-- Nav Item - Gestion Avis -->
        <li class="nav-item">
            <a class="nav-link" href="gestion_avis.php">
                <i class="fa-regular fa-star"></i>
                <span>Gestion Avis</span></a>
        </li>

        <!-- Divider -->
        <hr class="sidebar-divider d-none d-md-block">

        <!-- Sidebar Toggler (Sidebar) -->
        <div class="text-center d-none d-md-inline">
            <button class="rounded-circle border-0" id="sidebarToggle"></button>
        </div>


    </ul>
    <!-- End of Sidebar -->