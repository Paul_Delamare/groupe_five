<?php
session_start();

require '../inc/pdo.php';
require '../inc/function.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['status'] == 'admin') {

    }
} else {
    header('Location: 404.php');
}


$users = getAllUser();
?>
<?php include ('inc/sidebar.php'); ?>
<?php include ('inc/header.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Gestion Utilisateur</h1>
                    <p class="mb-4">Cette table vous permet de gerer les utilisateurs et de les modifier ou les bannir.</p>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 style="padding-bottom: 1rem" class="m-0 font-weight-bold text-primary">Utilisateurs</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prenom</th>
                                        <th>Email</th>
                                        <th>Age</th>
                                        <th>Genre</th>
                                        <th>Status</th>
                                        <th>Date de création</th>
                                        <th>Dernière modification</th>
                                        <th>Modifier/Supprimer</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Nom</th>
                                        <th>Prenom</th>
                                        <th>Email</th>
                                        <th>Age</th>
                                        <th>Genre</th>
                                        <th>Status</th>
                                        <th>Date de création</th>
                                        <th>Dernière modification</th>
                                        <th>Modifier/Supprimer</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php foreach ($users as $user) { ?>
                                        <tr>
                                            <td><?= $user['name'] ?></td>
                                            <td><?= $user['prenom'] ?></td>
                                            <td><?= $user['email'] ?></td>
                                            <td><?= $user['age'] ?></td>
                                            <td><?= $user['genre'] ?></td>
                                            <td><?= $user['status'] ?></td>
                                            <td><?= $user['created_at'] ?></td>
                                            <td><?= $user['modified_at'] ?></td>
                                            <td style="display: flex; justify-content: space-between">
                                                <a title="Editer" style="font-size: 1.5rem" href="edituser.php?id=<?= $user['id']; ?>"><i class="fa-solid fa-pen-to-square"></i></a>
                                                <a title="Administrateur" style="font-size: 1.5rem" href="adminuser.php?id=<?= $user['id']; ?>"><i class="fa-solid fa-user-plus"></i></a>
                                                <a title="Utilisateur" style="font-size: 1.5rem" href="republishuser.php?id=<?= $user['id']; ?>"><i class="fa-solid fa-user"></i></a>
                                                <a title="Bannir" style="font-size: 1.5rem" href="deleteuser.php?id=<?= $user['id']; ?>"<i class="fa-solid fa-user-xmark"></i></a>
                                            </td>

                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php include ('inc/footer.php'); ?>