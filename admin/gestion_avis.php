<?php
session_start();

require '../inc/pdo.php';
require '../inc/function.php';
require '../inc/validation.php';
require '../inc/request.php';

if (isLogged()) {
    if ($_SESSION['verifLogin']['status'] == 'admin') {

    }
} else {
    header('Location: ../404.php');
}


$avis = getAllAvis();
?>
<?php include ('inc/sidebar.php'); ?>
<?php include ('inc/header.php'); ?>

                <!-- Begin Page Content -->
                <div class="container-fluid">

                    <!-- Page Heading -->
                    <h1 class="h3 mb-2 text-gray-800">Gestion Avis</h1>
                    <p class="mb-4">Cette table vous permet de gerer les avis poster afin d'offrir une meilleure moderation.</p>

                    <!-- DataTales Example -->
                    <div class="card shadow mb-4">
                        <div class="card-header py-3">
                            <h6 class="m-0 font-weight-bold text-primary">Avis</h6>
                        </div>
                        <div class="card-body">
                            <div class="table-responsive">
                                <table class="table table-bordered" id="dataTable" width="100%" cellspacing="0">
                                    <thead>
                                    <tr>
                                        <th>Utilisateur</th>
                                        <th>Contenu</th>
                                        <th>Status</th>
                                        <th>Date de création</th>
                                        <th>Publier/Depublier</th>
                                    </tr>
                                    </thead>
                                    <tfoot>
                                    <tr>
                                        <th>Utilisateur</th>
                                        <th>Contenu</th>
                                        <th>Status</th>
                                        <th>Date de création</th>
                                        <th>Publier/Depublier</th>
                                    </tr>
                                    </tfoot>
                                    <tbody>
                                    <?php foreach ($avis as $avi) { ?>
                                        <tr>
                                            <td><?= $avi['name'] ?></td>
                                            <td><?= $avi['content'] ?></td>
                                            <td><?= $avi['status_avis'] ?></td>
                                            <td><?= $avi['created_at'] ?></td>
                                            <td style="display: flex; justify-content: space-between">
                                                <a title="Publier" style="font-size: 1.5rem" href="republishavis.php?id=<?= $avi['id']; ?>"><i class="fa-solid fa-plus"></i></a>
                                                <a title="Dépublier" style="font-size: 1.5rem" href="deleteavis.php?id=<?= $avi['id']; ?>"><i class="fa-solid fa-xmark"></i></a>
                                            </td>
                                        </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>

                </div>
                <!-- /.container-fluid -->

            </div>
            <!-- End of Main Content -->

<?php include ('inc/footer.php'); ?>