<?php
require 'pdo.php';

function foundAvis($n)
{
    global $pdo;
    $sql = "SELECT * FROM avis WHERE status_avis LIKE 'publish' ORDER BY created_at DESC LIMIT 1 OFFSET $n";
    $query = $pdo->prepare($sql);
    $query->execute();
    $avis = $query->fetchAll();
    echo '<div class="avis_publish_last"><p><i class="fa-solid fa-quote-left"></i>  ';
    if (empty($avis)) {
        echo "Pas encore d'avis ont été publiés...";
    } else {
        echo ucfirst($avis[0]['content']);
    }
    echo '  <i class="fa-solid fa-quote-right"></i></p></div>';
}

function getAllVaccin() {
global $pdo;
$sql = "SELECT * FROM vaccin";
$query = $pdo->prepare($sql);
$query->execute();
return $query->fetchAll();
}

function insertVaccin($title, $content)
{
    global $pdo;
    $sql = "INSERT INTO vaccin (title, content,created_at) 
                VALUES (:title,:content,NOW())";
    $query = $pdo->prepare($sql);
    $query->bindValue('title', $title, PDO::PARAM_STR);
    $query->bindValue('content', $content, PDO::PARAM_STR);
    $query->execute();
    return $pdo->lastInsertId();
}

function getVaccinById($id) {
    global $pdo;
    $sql = "SELECT * FROM vaccin WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}

function updateVaccin($id, $title, $content)
{
    global $pdo;
    $sql = "UPDATE vaccin SET title = :title,content = :content,modified_at = NOW() WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('title', $title, PDO::PARAM_STR);
    $query->bindValue('content', $content, PDO::PARAM_STR);
    $query->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
}

function getAllUser() {
    global $pdo;
    $sql = "SELECT * FROM user";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function getAllContact() {
    global $pdo;
    $sql = "SELECT * FROM contact";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function getContactById($id) {
    global $pdo;
    $sql = "SELECT * FROM contact WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}

function getAllAvis() {
    global $pdo;
    $sql = "SELECT A.*, U.name
            FROM avis A
            INNER JOIN user U
                ON A.user_id = U.id";

    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function getAvisById($id) {
    global $pdo;
    $sql = "SELECT * FROM avis WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}

function getAllVaccinEnCours() {
    global $pdo;
    $sql = "SELECT S.*, U.name, V.title
            FROM vaccin_status S
            INNER JOIN user U
                ON S.user_id = U.id
            INNER JOIN  vaccin V
                ON S.vaccin_id = V.id";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function getVaccinIdEffectue($id){
    global $pdo;
    $page=1;
    $itemPerPage=4;
    $offset=0;
    if(!empty($_GET['page'])) {
        $page = $_GET['page'];
        $offset = ($page - 1) * $itemPerPage;
    }
    $sql = "SELECT S.*, U.name, V.title
            FROM vaccin_status S
            INNER JOIN user U
                ON S.user_id = U.id
            INNER JOIN  vaccin V
                ON S.vaccin_id = V.id
                WHERE S.user_id=:id AND S.date_done!='NULL' ORDER BY date_done DESC LIMIT $itemPerPage OFFSET $offset";
    if (isset($_GET['search']) AND !empty($_GET['search'])) {
        $recherche = htmlspecialchars($_GET['search']);
        $sql = "SELECT S.*, U.name, V.title
            FROM vaccin_status S
            INNER JOIN user U
                ON S.user_id = U.id
            INNER JOIN  vaccin V
                ON S.vaccin_id = V.id
                WHERE S.user_id=:id AND s.date_done!='NULL'
                 AND V.title LIKE '%$recherche%'
                LIMIT $itemPerPage 
                OFFSET $offset";
    }
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id);
    $query->execute();
    return $query->fetchAll();
}

function getVaccinIdAttente($id){
    global $pdo;
    $page=1;
    $itemPerPage=4;
    $offset=0;
    if(!empty($_GET['page'])) {
        $page = $_GET['page'];
        $offset = ($page - 1) * $itemPerPage;
    }
    $sql = "SELECT S.*, U.name, V.title
            FROM vaccin_status S
            INNER JOIN user U
                ON S.user_id = U.id
            INNER JOIN  vaccin V
                ON S.vaccin_id = V.id
                WHERE S.user_id=:id AND S.date_todo!='NULL' ORDER BY date_todo ASC LIMIT $itemPerPage OFFSET $offset";
    if (isset($_GET['search']) AND !empty($_GET['search'])) {
        $recherche = htmlspecialchars($_GET['search']);
        $sql = "SELECT S.*, U.name, V.title
            FROM vaccin_status S
            INNER JOIN user U
                ON S.user_id = U.id
            INNER JOIN  vaccin V
                ON S.vaccin_id = V.id
                WHERE S.user_id=:id AND s.date_todo!='NULL'
                AND V.title LIKE '%$recherche%'
                LIMIT $itemPerPage 
                OFFSET $offset";
    }
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id);
    $query->execute();
    return $query->fetchAll();
}

//function getVaccinResearch($id){
//    $page = 1;
//    $itemPerPage = 4;
//    $offset = 0;
//    if(!empty($_GET['page'])) {
//        $page = $_GET['page'];
//        $offset = ($page - 1) * $itemPerPage;
//    }
//    global $pdo;
//    $sql = "SELECT S.*, U.name, V.title
//            FROM vaccin_status S
//            INNER JOIN user U
//                ON S.user_id = U.id
//            INNER JOIN  vaccin V
//                ON S.vaccin_id = V.id
//                WHERE S.user_id=:id
//                LIMIT $itemPerPage
//                OFFSET $offset";
//
//    $query = $pdo->prepare($sql);
//    $query->bindValue('id',$id);
//    $query->execute();
//    return $query->fetchAll();
//}

function getCount($id){
    global $pdo;
    $sql = "SELECT S.*, U.name, V.title
            FROM vaccin_status S
            INNER JOIN user U
                ON S.user_id = U.id
            INNER JOIN  vaccin V
                ON S.vaccin_id = V.id
                WHERE S.user_id=:id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id);
    $query->execute();
    return $query->fetchAll();
}

function getVaccinProfil($id){
    global $pdo;
    $sql = "SELECT S.*, U.name, V.title
                FROM vaccin_status S
                INNER JOIN user U
                    ON S.user_id = U.id
                INNER JOIN  vaccin V
                    ON S.vaccin_id = V.id WHERE S.user_id= :id AND s.date_done!= 'NULL' ORDER BY s.date_done ASC";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id);
    $query->execute();
    return $query->fetchAll();
}

function getNextVaccin($id){
    global $pdo;
    $sql = "SELECT S.*, U.name, V.title
                FROM vaccin_status S
                INNER JOIN user U
                    ON S.user_id = U.id
                INNER JOIN  vaccin V
                    ON S.vaccin_id = V.id WHERE S.user_id= :id AND s.date_todo!='NULL' ORDER BY s.date_todo DESC ";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id);
    $query->execute();
    return $query->fetchAll();
}

function getUserById($id) {
    global $pdo;
    $sql = "SELECT * FROM user WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}

function updateUser($id, $name, $prenom, $email, $genre, $age)
{
    global $pdo;
    $sql = "UPDATE user SET name = :name,prenom = :prenom,email = :email, genre = :genre, age = :age,modified_at = NOW() WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('name', $name, PDO::PARAM_STR);
    $query->bindValue('prenom', $prenom, PDO::PARAM_STR);
    $query->bindValue('email', $email, PDO::PARAM_STR);
    $query->bindValue('genre', $genre, PDO::PARAM_STR);
    $query->bindValue('age', $age, PDO::PARAM_STR);
    $query->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
}

function getVecById($id) {
    global $pdo;
    $sql = "SELECT S.*
            FROM vaccin_status S
            WHERE S.id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('id',$id, PDO::PARAM_INT);
    $query->execute();
    return $query->fetch();
}

function updateVec($id, $date_todo, $date_done, $doses)
{
    global $pdo;
    $sql = "UPDATE vaccin_status SET date_todo = :date_todo,date_done = :date_done,doses = :doses,modified_at = NOW() WHERE id = :id";
    $query = $pdo->prepare($sql);
    $query->bindValue('date_todo', $date_todo, PDO::PARAM_STR);
    $query->bindValue('date_done', $date_done, PDO::PARAM_STR);
    $query->bindValue('doses', $doses, PDO::PARAM_STR);
    $query->bindValue('id', $id, PDO::PARAM_INT);
    $query->execute();
}

function getAllVaccinEnCoursStatus() {
    global $pdo;
    $sql = "SELECT S.*
            FROM vaccin_status S
                WHERE S.status = 'effectué'";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function getAllVaccinEnCoursDateDone() {
    global $pdo;
    $sql = "SELECT S.*
            FROM vaccin_status S
                WHERE S.date_done != 'NULL'";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function getAllUserHomme() {
    global $pdo;
    $sql = "SELECT * FROM user WHERE genre = 'homme'";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
    echo json_encode($query);
}

function getAllUserFemme() {
    global $pdo;
    $sql = "SELECT * FROM user WHERE genre = 'femme'";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function getAllUserAutre() {
    global $pdo;
    $sql = "SELECT * FROM user WHERE genre = 'autre'";
    $query = $pdo->prepare($sql);
    $query->execute();
    return $query->fetchAll();
}

function insertVaccinEncoursToDo($date_todo, $vaccin_id, $user_id)
{
    global $pdo;
//    $allvaccinencours = getAllVaccinEnCours();
//    $userconnected = $_SESSION['verifLogin']['id'];
//
//    foreach ($allvaccinencours as $allvaccinencour) {
//        $allvaccin = $allvaccinencour['vaccin_id'];
//    }
    $sql="SELECT * FROM vaccin_status WHERE vaccin_id=:idva AND user_id = :id";
    $query= $pdo->prepare($sql);
    $query->bindValue('idva', $vaccin_id);
    $query->bindValue('id', $user_id);
    $query->execute();
    $verifId= $query->fetch();

    if (!empty($verifId)) {

        $sql="UPDATE vaccin_status 
                SET date_todo= :date , status='en attente'
                WHERE vaccin_id = :idva AND
                user_id = :id";
        $query = $pdo->prepare($sql);
        $query->bindValue('date', $date_todo, PDO::PARAM_STR);
        $query->bindValue('idva', $vaccin_id, PDO::PARAM_STR);
        $query->bindValue('id', $user_id, PDO::PARAM_STR);
        $query->execute();
        return $pdo->lastInsertId();

    } else {
        global $pdo;
        $dose=0;

        $sql = "INSERT INTO vaccin_status (date_todo, doses, vaccin_id, user_id, status, created_at) 
                VALUES (:date_todo, :doses, :vaccin_id,:user_id, 'en attente', NOW())";
        $query = $pdo->prepare($sql);
        $query->bindValue('date_todo', $date_todo, PDO::PARAM_STR);
        $query->bindValue('doses', $dose, PDO::PARAM_STR);
        $query->bindValue('vaccin_id', $vaccin_id, PDO::PARAM_STR);
        $query->bindValue('user_id', $user_id, PDO::PARAM_STR);
        $query->execute();
        return $pdo->lastInsertId();
    }
}

function insertVaccinEncoursDone($date_done, $dosebase, $vaccin_id, $user_id)
{
    global $pdo;
//    $allvaccinencours = getAllVaccinEnCours();
//    $userconnected = $_SESSION['verifLogin']['id'];
//
//    foreach ($allvaccinencours as $allvaccinencour) {
//        $allvaccin = $allvaccinencour['vaccin_id'];
//    }
//    debug($allvaccin);
    $sql="SELECT * FROM vaccin_status WHERE vaccin_id=:idva AND user_id = :id";
    $query= $pdo->prepare($sql);
    $query->bindValue('idva', $vaccin_id);
    $query->bindValue('id', $user_id);
    $query->execute();
    $verifId= $query->fetch();


    if ( !empty($verifId)) {
        global $pdo;
        $sql = "SELECT doses
                            FROM vaccin_status
                            WHERE vaccin_id = :vaccin_id AND
                user_id = :user_id";
        $query = $pdo->prepare($sql);
        $query->bindValue('vaccin_id', $vaccin_id, PDO::PARAM_STR);
        $query->bindValue('user_id', $user_id, PDO::PARAM_STR);
        $query->execute();
        $dosestable = $query->fetchAll();

        $doses = $dosestable['0']['doses'];
        $newdose= $doses +$dosebase;

        $sql="UPDATE vaccin_status 
                SET doses= :newdose , date_done = :date_done
                WHERE vaccin_id = :vaccin_id AND
                user_id = :user_id";
        $query = $pdo->prepare($sql);
        $query->bindValue('date_done', $date_done, PDO::PARAM_STR);
        $query->bindValue('newdose', $newdose, PDO::PARAM_STR);
        $query->bindValue('vaccin_id', $vaccin_id, PDO::PARAM_STR);
        $query->bindValue('user_id', $user_id, PDO::PARAM_STR);
        $query->execute();
        return $pdo->lastInsertId();

    } else {
        global $pdo;

        $sql = "INSERT INTO vaccin_status (date_done, doses, vaccin_id, user_id, status, created_at) 
                VALUES (:date_done, :doses, :vaccin_id,:user_id, 'effectué', NOW())";
        $query = $pdo->prepare($sql);
        $query->bindValue('date_done', $date_done, PDO::PARAM_STR);
        $query->bindValue('doses', $dosebase, PDO::PARAM_STR);
        $query->bindValue('vaccin_id', $vaccin_id, PDO::PARAM_STR);
        $query->bindValue('user_id', $user_id, PDO::PARAM_STR);
        $query->execute();
        return $pdo->lastInsertId();
    }
}