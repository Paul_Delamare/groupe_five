<footer id="colophon">
    <div class="wrap">
        <div class="arrow_top">
            <a href="#retop">
                <i class="fa-solid fa-circle-chevron-up"></i>
            </a>
        </div>
        <div class="logo_footer">
            <a href="index.php">
                <img src="asset/image/logo.png" alt="Logo Footer">
            </a>
        </div>
        <ul>
            <?php if (isLogged()){
                echo '<li><a href="profil.php">Mon Profil</a></li>';
                echo '<li><a href="vaccin.php">Mes Vaccins</a></li>';
            }?>
            <li><a href="contact.php">Contact</a></li>
            <li><a href="mentions.php">Mentions légales</a></li>
        </ul>
    </div>
</footer>


</body>
</html>