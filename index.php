<?php
session_start();
require 'inc/pdo.php';
require 'inc/function.php';
require 'inc/request.php';
//require 'inc/validation.php';
if (!empty($_SESSION)){
    if ($_SESSION['verifLogin']['status']=='draft'){
        header('Location: ban.php');
    }
}


$avis=false;
include 'inc/header.php';
?>
<section id="home">
    <div class="wrap2">
        <div class="home_titre">
            <h1>rapide, simple, pratique, renseignez facilement votre carnet de santé de manière durable et sécurisée !</h1>
        </div>
        <div class="home_nav">
            <ul>
                <?php if (isLogged()){
                    echo '<li><a href="profil.php">Mon profil</a></li>';
                    echo '<li><a href="vaccin.php">Mes vaccins</a></li>';
                }else{
                    echo '<li><a href="login.php">Se connecter</a></li>';
                    echo "<li><a href=\"register.php\">S'inscrire</a></li>";
                }
                ?>
            </ul>
        </div>
    </div>
</section>
<section id="video">
    <div class="wrap">
        <div class="home_avis">
            <div class="avis_display">
                <div class="avis_publish">
                    <?php
                    foundAvis(0);
                    ?>
                </div>
                <div class="avis_publish">
                    <?php
                    foundAvis( 1);
                    ?>
                </div>
                <div class="avis_publish">
                    <?php
                    foundAvis(2);
                    ?>
                </div>
            </div>
        </div>
        <div class="home_video">
            <h1>Tout est simple !</h1>
            <div class="video_width">
                <iframe src="https://www.youtube.com/embed/DUJoCfirHwE" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>            </div>
        </div>
    </div>
</section>
<?php

include 'inc/footer.php';
