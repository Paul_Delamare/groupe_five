<?php
session_start();
if (!empty($_SESSION)){
    header('Location: 404.php');
}
require('inc/pdo.php');
require('inc/function.php');
require('inc/request.php');
require('inc/validation.php');
$errors = array();

if (!empty($_POST['submit'])){
    $email=cleanXss('email');
    $password=cleanXss('password');
    $errors= validationText($errors, $email, 'email', 3, 150);
    $errors= validationText($errors, $password, 'password', 6, 255);

    $sql= "SELECT * FROM user WHERE email= :email";
    $query= $pdo->prepare($sql);
    $query->bindValue('email', $email);
    $query->execute();
    $verifLogin = $query->fetch();
    if (!empty($verifLogin)){
        if (password_verify($password, $verifLogin['password'])){
            $_SESSION['verifLogin']=array(
                'id'=>$verifLogin['id'],
                'name'=>$verifLogin['name'],
                'prenom'=>$verifLogin['prenom'],
                'email'=>$verifLogin['email'],
                'status'=>$verifLogin['status'],
                'ip'=>$_SERVER['REMOTE_ADDR']
            );
            header('Location: profil.php');
        }else{
            $errors['login']='Credential';
        }
    }else{
        $errors['login']='credential';
    }
}
include('inc/header.php'); ?>
<section id="login">
    <div class="wrap3">
        <div class="login_form">
            <div class="log_gauche">
                <div class="gauche_info">
                    <h2>S'inscrire</h2>
                    <h3>Pas encore de compte ?</h3>
                    <i class="fa-solid fa-chevron-down"></i>
                </div>
                <a href="register.php">S'inscrire</a>
            </div>
            <div class="log_droite">
                <h2>Se connecter</h2>
                <form action="" method="post" class="wrapform" novalidate>
                    <div class="log_email log_input">
                        <input name="email" id="email" placeholder="Email" type="text">
                        <span class="errors"><?php viewError($errors, 'email'); ?></span>
                    </div>

                    <div class="log_mdp log_input">
                        <input name="password" id="password" placeholder="Mot de passe" type="password">
                        <span class="errors"><?php viewError($errors, 'password'); ?></span>
                    </div>
                    <div class="motdepasse">
                        <a href="motdepasse.php">Mot de passe oublié ?</a>
                    </div>
                    <div class="log_submit">
                        <input name="submit" type="submit" value="Se connecter">
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>
<?php include('inc/footer.php');